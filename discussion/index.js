 console.log("JS: Basic Function");

//  function declaration // function name
// function name requires open and close parentheses beside it
 function printName(){ // codeblock
    console.log("My name is John"); // function statement
 }

 printName();

//  Function Declaration
    // function statement defines a fuction with parameters
    // function keyword - use to define a javascript function
    // function name - is used so we are able to call/invoke a declared function
    // function code block {} - the state which compromise the body of the function. This is where the code to be executed.


// [HOISTING]
// invoke the function before/after the function declaration
declaredFuntion();
// make sure the function is existing whenever we call/invoke a function

 function declaredFuntion(){
    console.log("Hello World!");
 };

//  [Function Expression]
//  A function can be also stored in a variable. That is called as function expression

let variableFunction = function(){
    console.log("Hello World again!");
};

variableFunction();

let functionExpression = function funcName(){
    console.log("Hello from the other side.");
};

functionExpression();

console.log("-------");
console.log("Reassigning Functions");

declaredFuntion();
declaredFuntion = function(){
    console.log("updated declaredFunction");
};

declaredFuntion();

functionExpression = function(){
    console.log("updated function expression");
};

functionExpression();

// Constant function
const constantFunction =  function(){
    console.log("Initialized with const.");
};

constantFunction();
console.log("-------");
console.log("Function Scoping");

{
    let localVar = "Armando Perez";
    console.log(localVar);
}

// console.log(localVar);

let globalVar = "Mr Worldwide";
console.log(globalVar);

{
    console.log(globalVar);
}

function showNames(){
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

function myNewFunction(){
    let name = "Jane"

    function nestedFunction(){
        let nestedName = "John";
        console.log(name);
    }

    nestedFunction();
    console.log(nestedFunction);
}

myNewFunction();

let globalName = "Zuitt";

function myNewFunction2(){
    let nameInside = "Renz";
    console.log(globalName);
}

myNewFunction2();

// function showSampleAlert(){
//     alert("Hello user");
// };

// showSampleAlert();

// alert messages inside a function will only execute whenever we call/invoke the function

console.log("-------");
console.log("I will only log in the console when the alert is dismissed");

// let samplePrompt = prompt("Enter your Full Name:");
// // console.log(samplePrompt);
// // // prompt("");

// // console.log(typeof samplePrompt);

// console.log("Hello, " + samplePrompt);

// function printWelcomeMessage(){
//     let firstName = prompt("Enter your first name: ");
//     let lastName = prompt("Enter your last name: ");

//     console.log("Hello, " + firstName + " " + lastName);
// }

// printWelcomeMessage();

function getCourses(){
    let courses = ["Science 101", "Math 101", "English 101"];
    console.log(courses); 
}

getCourses();


// function get(){
//     let name = "Jamie";
//     console.log(name);
// };

// get();

// function foo(){
//     console.log(25%5);
// };

// foo()

function displayCarInfo(){
    console.log("Brand: Toyota");
    console.log("Type: Sedan");
    console.log("Price: P1,999,999");
};

displayCarInfo();
