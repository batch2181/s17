
/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function displayInfo(){
		let fullName = prompt("Enter your full name:");
		let age = prompt("Enter your age: ");
		let location = prompt("Enter your location: ");

		console.log("Hello, " + fullName);
		console.log("You are "+ age + " years old.");
		console.log("You live in " + location);
	}

	displayInfo();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	console.log("Your Top 5 Bands:")
	function displayFavoriteBands(){
		let favBand1 = "My Chemical Romance";
		let favBand2 = "Green Day";
		let favBand3 = "Yellow Card";
		let favBand4 = "All Time Low";
		let favBand5 = "Fall Out Boys";
		
		console.log("1. " + favBand1);
		console.log("2. " + favBand2);
		console.log("3. " + favBand3);
		console.log("4. " + favBand4);
		console.log("5. " + favBand5);
	};

	displayFavoriteBands();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	console.log("Your Top 5 Movies of All Time:")
	function displayFavoriteMovies(){

		let favMovie1 = "The Conjuring I";
		let favMovie1Rating = "86%";
		let favMovie2 = "The Conjuring II";
		let favMovie2Rating = "80%";
		let favMovie3 = "The Conjuring III";
		let favMovie3Rating = "56%";
		let favMovie4 = "Anabelle"; 
		let favMovie4Rating = "28%";
		let favMovie5 = "Anabelle: Creation";
		let favMovie5Rating = "70%";

		let rottenRating = "Rotten Tomatoes Rating: ";

		console.log("1. " + favMovie1);
		console.log(rottenRating + favMovie1Rating);
		console.log("2. " + favMovie2);
		console.log(rottenRating + favMovie2Rating);
		console.log("3. " + favMovie3);
		console.log(rottenRating + favMovie3Rating);
		console.log("4. " + favMovie4);
		console.log(rottenRating + favMovie4Rating);
		console.log("5. " + favMovie5);
		console.log(rottenRating + favMovie5Rating);

	};

	displayFavoriteMovies();


/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: " + friend1 + ", " + friend2 + ", " + friend3)

};

printFriends();

// console.log(friend1);
// console.log(friend2);
